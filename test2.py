from flask import Flask, Response
from flask_redis import FlaskRedis
import datetime

app = Flask(__name__)
redis_store = FlaskRedis(app)
redis_key = "my_incr"


def series():
    for i in range(1, 999):
        yield i


def make_get_or_rotate_series():
    today = datetime.date.today()
    current_series = series()

    def get_or_rotate_series(current_day=None):
        nonlocal today, current_series
        current_day = current_day if current_day else datetime.date.today()

        if today != current_day:
            today = current_day
            current_series = series()

        return current_series
    return get_or_rotate_series


get_series = make_get_or_rotate_series()


@app.route('/eRxBot/Increment', methods=['POST', 'GET'])
def gen():
    val = str(next(get_series()))
    print(val)
    res = Response(val)
    return res


@app.route('/eRxBot/Increment-Redis', methods=['POST', 'GET'])
def gen_redis():
    val = redis_store.incr(redis_key)
    res = Response(str(val))
    return res


if __name__ == "__main__":
    # tests
    test_get_series = make_get_or_rotate_series()
    assert next(test_get_series()) == 1
    assert next(test_get_series()) == 2
    tomorrow = datetime.date.today() + datetime.timedelta(days=1)
    assert next(test_get_series(tomorrow)) == 1
    assert next(test_get_series(tomorrow)) == 2

    app.run()